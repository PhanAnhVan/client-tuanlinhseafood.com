import { Component, Input, OnInit } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { ToastrService } from 'ngx-toastr'
import { Globals } from '../../../../globals'
import { OwlOptions, SlidesOutputData } from 'ngx-owl-carousel-o'

@Component({
    selector: 'app-detail-product',
    templateUrl: './detail-product.component.html',
    styleUrls: ['./detail-product.component.css']
})
export class DetailProductComponent implements OnInit {
    @Input('data') data: any

    public connect

    public item: any = {}

    public listImages = { data: [], cached: [], active: 0 }

    public activeSlides: SlidesOutputData

    public showOwl: boolean = false

    public widthOwl: number

    public selected: any

    width: number = 0

    public link: string = ''

    public token: any = {
        getProductDetail: 'api/getproductdetail',

        getRelatedProduct: 'api/relatedproduct'
    }

    constructor(
        public route: ActivatedRoute,

        public router: Router,

        public toastr: ToastrService,

        public globals: Globals
    ) {
        this.width = innerWidth
    }

    ngOnInit() {
        let listimages = this.data.listimages && this.data.listimages.length > 5 ? JSON.parse(this.data.listimages) : []

        this.listImages.cached = listimages

        if (this.data.images && this.data.images.length > 4) {
            this.listImages.cached = [this.data.images].concat(listimages)
        } else {
            this.data.images = listimages.length > 0 ? listimages[0] : ''
        }

        this.listImages.data = Object.values(this.listImages.cached)

        setTimeout(() => {
            this.widthOwl = document.getElementById('owl-carousel').offsetWidth / (this.width > 415 ? 5 : 2) - (this.width > 415 ? 15 : 20)
            this.showOwl = true
        }, 300)
    }

    isActive(images, index) {
        this.item.images = images
        this.listImages.active = index
    }

    public library = {
        libraryOptions: {
            loop: true,
            autoplayTimeout: 8000,
            autoplaySpeed: 1500,
            mouseDrag: true,
            touchDrag: true,
            pullDrag: false,
            dots: false,
            navSpeed: 500,
            dotsData: true,
            items: 1,
            navText: ["<i class='fas fa-angle-left'></i>", "<i class='fas fa-angle-right'></i>"],
            nav: true
        },

        getPassedData: (data: SlidesOutputData) => {
            this.activeSlides = data
            this.selected = data.slides[0] ? data.slides[0].id : ''
            this.data.images = this.selected
            setTimeout(() => {
                this.library.onClickImageChild(this.selected + '-child')
            }, 200)
        },

        onClickImageChild: id => {
            let items: any = document.querySelectorAll('.img-child')
            for (let i = 0; i < items.length; i++) {
                items[i].style.opacity = 0.5
                items[i].classList.remove('border')
                items[i].classList.remove('border-danger')
            }
            document.getElementById(id).style.opacity = '1'
            document.getElementById(id).classList.add('border')
            document.getElementById(id).classList.add('border-danger')
        }
    }

    related = {
        data: [],
        relatedOptions: {
            autoWidth: true,
            mouseDrag: true,
            touchDrag: true,
            pullDrag: true,
            margin: 10,
            responsive: {
                0: {
                    items: 2
                },
                740: {
                    items: 3
                },
                940: {
                    items: 5
                },
                1140: {
                    items: 5
                }
            },
            dots: false,
            nav: true,
            navText: ["<i class='fas fa-angle-left'></i>", "<i class='fas fa-angle-right'></i>"]
        }
    }
}
