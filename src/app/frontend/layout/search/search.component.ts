import { Component, OnDestroy, OnInit, SimpleChanges } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { PageChangedEvent } from 'ngx-bootstrap'
import { Globals } from '../../../globals'
import { TableService } from '../../../services/integrated/table.service'
import { ToslugService } from '../../../services/integrated/toslug.service'

@Component({
    selector: 'app-search',
    templateUrl: './search.component.html',
    styleUrls: ['./search.component.scss'],
    providers: [ToslugService, TableService]
})
export class SearchComponent implements OnInit, OnDestroy {
    private connect

    width: number = innerWidth

    public search: any = { name: '', show: -1 }

    public token: any = {
        getdata: 'api/search'
    }

    public tableContent = new TableService()

    constructor(
        private route: ActivatedRoute,
        public globals: Globals,
        private router: Router,
        private toSlug: ToslugService,
        public cwstable: TableService
    ) {
        this.tableContent._ini({
            data: [],
            keyword: 'getdataContentSearch',
            count: this.Pagination.itemsPerPage,
            sorting: { field: 'maker_date', sort: 'DESC', type: 'date' }
        })

        this.tableContent.sorting.field = ''

        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case 'getdata':
                    if (res.status !== 1) return

                    this.search.show = res.data.contents.length || res.data.products.length ? 1 : 0

                    this.tableContent._concat(res.data.contents, true)

                    this.products.data = res.data.products
                    break

                default:
                    break
            }
        })
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            this.search.name = params.keywords || ''

            this.search.show = params.keywords ? true : false

            if (params.keywords && params.keywords != '') {
                this.globals.send({ path: this.token.getdata, token: 'getdata', params: { keywords: params.keywords } })
            }
        })
    }

    ngOnDestroy() {
        this.connect.unsubscribe()
    }

    searchAgain() {
        if (this.search.name != '') {
            this.router.navigate(['/' + this.globals.language.getCode() + '/search/' + this.toSlug._ini(this.search.name)])

            this.search.name = ''

            document.getElementById('searchAgain').blur()
        }
    }

    public Pagination = {
        maxSize: 5,

        itemsPerPage: 12,

        changeContent: (event: PageChangedEvent) => {
            const startItem = (event.page - 1) * event.itemsPerPage

            const endItem = event.page * event.itemsPerPage

            this.tableContent.data = this.tableContent.dataList.slice(startItem, endItem)

            window.scroll({ top: 0 })
        }
    }

    products = {
        data: []
    }
}
