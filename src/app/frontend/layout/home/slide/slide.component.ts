import { Component, OnDestroy, OnInit } from '@angular/core'
import { SlidesOutputData } from 'ngx-owl-carousel-o'
import { Globals } from '../../../../globals'
import { SLIDE_ARROW_LEFT, SLIDE_ARROW_RIGHT } from '../../../core/constants'

@Component({
    selector: 'app-slide',
    templateUrl: './slide.component.html',
    styleUrls: ['./slide.component.scss']
})
export class SlideComponent implements OnInit, OnDestroy {
    private connect

    public activeSlides: SlidesOutputData

    public selected: any

    width: number = innerWidth

    constructor(public globals: Globals) {
        this.connect = this.globals.result.subscribe((response: any) => {
            switch (response.token) {
                case 'getslide':
                    this.slide.data = response.data

                    this.slideOptions.dots = this.slide.data.length > 1 ? true : false
                    break

                default:
                    break
            }
        })
    }

    ngOnInit() {
        this.slide.send()
    }

    ngOnDestroy() {
        this.connect.unsubscribe()
    }

    public slide = {
        token: 'api/home/slide',
        data: [],
        send: () => {
            this.globals.send({ path: this.slide.token, token: 'getslide', params: { type: 1 } })
        }
    }

    public slideOptions = {
        loop: true,
        autoplay: true,
        items: 1,
        dots: true,
        nav: true,
        navText: [`<img src="${SLIDE_ARROW_LEFT}" alt="Previous" />`, `<img src="${SLIDE_ARROW_RIGHT}" alt="Next">`]
        // animateOut: 'fadeOut'
    }
}
