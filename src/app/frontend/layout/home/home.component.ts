import { Component, OnDestroy, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { Globals } from '../../../globals'
import { GET_COMPANY_INFO, GetCompanyInfo } from '../../core/constants/api'
import { GetStorage, CompanyInfo } from '../../core/constants/store'

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit, OnDestroy {
    private connect
    width: number = innerWidth
    company = <any>{}

    constructor(public globals: Globals, public router: Router) {
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case 'getAboutus':
                    this.about.data = res.data
                    break

                case GetCompanyInfo:
                    this.company = res.data
                    break

                default:
                    break
            }
        })
    }

    ngOnInit() {
        this.about.send()

        const companyInfo = GetStorage(CompanyInfo) || false
        if (companyInfo) {
            this.company = companyInfo
        } else {
            this.globals.send({
                path: GET_COMPANY_INFO,
                token: GetCompanyInfo
            })
        }
    }

    ngOnDestroy() {
        this.connect.unsubscribe()
    }

    public about = {
        token: 'api/getAboutus',
        data: <any>{},
        send: () => {
            this.globals.send({ path: this.about.token, token: 'getAboutus' })
        }
    }
}
