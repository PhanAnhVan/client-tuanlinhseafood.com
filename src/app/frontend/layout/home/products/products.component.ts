import { Component, OnDestroy, OnInit } from '@angular/core'
import { Globals } from './../../../../globals'
import { GetProductsPin, GET_PRODUCTS_PIN } from './../../../core/constants/api'

@Component({
    selector: 'home-products',
    templateUrl: './products.component.html'
})
export class HomeProductsComponent implements OnInit, OnDestroy {
    private connect
    private width: number = innerWidth

    constructor(public globals: Globals) {
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case GetProductsPin:
                    if (res.status !== 1 || res.data.list.length === 0) return

                    this.products.data = res.data

                    this.products.compare(this.products.data.list)
                    break

                default:
                    break
            }
        })
    }

    ngOnInit() {
        this.products.send()
    }

    ngOnDestroy() {
        this.connect.unsubscribe()
    }

    products = {
        data: <any>{},
        limit: 16,
        send: () =>
            this.globals.send({
                path: GET_PRODUCTS_PIN,
                token: GetProductsPin,
                params: { limit: this.products.limit }
            }),
        compare: data => {
            let list = []
            if (data && data.length > 0) {
                let length = data.length / 2
                for (let i = 0; i < length; i++) {
                    list[i] = []
                    if (list[i].length < 2) {
                        list[i] = data.splice(0, 2)
                    }
                    data = Object.values(data)
                }
            }
            return (this.products.data.list = list)
        },
        options: {
            items: this.width > 1024 ? 4 : this.width > 425 ? 3 : 2,
            margin: 15,
            autoplay: false,
            dots: true
        }
    }
}
