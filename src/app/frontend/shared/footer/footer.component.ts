import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { TranslateService } from '@ngx-translate/core'
import { Globals } from '../../../globals'
import { GetCompanyInfo, GET_COMPANY_INFO } from '../../core/constants/api'
import { CompanyInfo, GetStorage } from '../../core/constants/store'

@Component({
    selector: 'app-footer',
    templateUrl: './footer.component.html'
})
export class FooterComponent implements OnInit {
    public connect
    company = <any>{}

    public type = 'password'

    public service: any = { data: [] }

    public policy: any = { data: [] }

    public about: any = { data: [] }

    width: number = 0

    public token: any = {
        menu: 'api/getmenu'
    }
    constructor(public globals: Globals, public translate: TranslateService, public router: Router) {
        this.width = innerWidth
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case 'menuFooterService':
                    this.service.data = res.data
                    break

                case 'menuFooterAbout':
                    this.about.data = res.data
                    break

                case 'menuFooterPolicy':
                    this.policy.data = res.data
                    break

                case GetCompanyInfo:
                    this.company = res.data
                    break

                default:
                    break
            }
        })
    }

    ngOnInit() {
        const companyInfo = GetStorage(CompanyInfo) || false
        if (companyInfo) {
            this.company = companyInfo
        } else {
            this.globals.send({
                path: GET_COMPANY_INFO,
                token: GetCompanyInfo
            })
        }
    }

    ngOnDestroy() {
        this.connect.unsubscribe()
    }

    getType = (link, type) => {
        switch (+type) {
            case 1:
            case 2:
            case 3:
            case 4:
                link = link
                break

            default:
                break
        }

        return link
    }
}
