import { Component, EventEmitter, HostListener, OnDestroy, OnInit, Output } from '@angular/core'
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router'
import { TranslateService } from '@ngx-translate/core'
import { Globals } from '../../../globals'
import { ToslugService } from '../../../services/integrated/toslug.service'
import { GET_COMPANY_INFO, GetCompanyInfo } from '../../core/constants/api'
import { GetStorage, CompanyInfo, RemoveStorage } from '../../core/constants/store'

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    providers: [ToslugService]
})
export class HeaderComponent implements OnInit, OnDestroy {
    @Output('loading') loading = new EventEmitter()

    public connect

    width: number = window.innerWidth

    public company: any = {}
    public hovermenu: boolean = true
    public mutiLang: any = { active: {}, data: [] }
    public path: any = ''
    public color: boolean = false

    constructor(
        public globals: Globals,
        public translate: TranslateService,
        public router: Router,
        public toSlug: ToslugService,
        public routerAtc: ActivatedRoute
    ) {
        if (window['fixmenu']) {
            window['fixmenu']()
        }

        let language = this.globals.language.get(false)
        this.mutiLang.data = this.globals.language.getData()
        if (this.mutiLang.data.length > 0) {
            this.mutiLang.data.filter(item => {
                if (+item.id == +language) {
                    this.mutiLang.active = item
                }
                return item
            })
        }

        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case GetCompanyInfo:
                    this.company = res.data
                    break

                default:
                    break
            }
        })
    }

    ngOnInit() {
        this.router.events.subscribe(event => {
            if (event instanceof NavigationEnd) {
                this.width > 768 ? '' : this.menuMobile.onMenu(true)
            }
        })

        const companyInfo = GetStorage(CompanyInfo) || false
        if (companyInfo) {
            this.company = companyInfo
        } else {
            this.globals.send({
                path: GET_COMPANY_INFO,
                token: GetCompanyInfo
            })
        }
    }

    ngOnDestroy() {
        this.connect.unsubscribe()
    }

    public menuMobile = {
        show: <boolean>true,

        onMenu: (show: boolean) => {
            this.menuMobile.show = show
            let elm2 = document.getElementById('menu-mobi')
            let headerCenter = document.getElementById('header-center')

            if (!this.menuMobile.show) {
                elm2.style.display = 'block'
                headerCenter.classList.add('navbar-fixed')
                document.body.style.overflowY = 'hidden'
            } else {
                document.body.style.overflowY = 'scroll'
                elm2.style.display = 'none'
                var currentScroll = document.documentElement.scrollTop || document.body.scrollTop
                if (currentScroll == 0) {
                    headerCenter.classList.remove('navbar-fixed')
                }
            }
        }
    }

    @HostListener('window:scroll', ['$event']) checkScroll() {
        let headerCenter = document.getElementById('header-center')
        var currentScroll = document.documentElement.scrollTop || document.body.scrollTop
        if (currentScroll > 0) {
            headerCenter.classList.add('navbar-fixed')
        } else {
            this.menuMobile.show == false ? '' : headerCenter.classList.remove('navbar-fixed')
        }
    }

    public search = {
        token: '',

        icon: <boolean>false,

        value: '',

        onSearch: () => {
            this.search.value = this.toSlug._ini(this.search.value.replace(/\s+/g, ' ').trim())

            if (this.search.check_search(this.search.value)) {
                this.router.navigate(['/' + this.globals.language.getCode() + '/search/' + this.search.value])

                document.getElementById('search').blur()
            }

            this.search.value = ''

            this.search.icon = false
        },

        check_search: value => {
            let skip = true
            skip =
                value.toString().length > 0 &&
                value != '' &&
                value != '-' &&
                value != '[' &&
                value != ']' &&
                value != '\\' &&
                value != '{' &&
                value != '}'
                    ? true
                    : false
            return skip
        },

        showSearch: () => {
            this.search.icon = !this.search.icon
        }
    }

    onLanguage = language_id => {
        this.mutiLang.data.filter(item => {
            if (+item.id == +language_id) {
                this.mutiLang.active = item
            }
            return item
        })
        this.globals.language.set(language_id, false)
        setTimeout(() => {
            // TODO: remove cached company info
            RemoveStorage(CompanyInfo)

            this.router.navigate(['/' + this.mutiLang.active.code])
        }, 500)
        this.loading.emit('')
    }
}
