import { Component, OnDestroy, OnInit } from '@angular/core'
import { DomSanitizer } from '@angular/platform-browser'
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router'
import { PageChangedEvent } from 'ngx-bootstrap'
import { Globals } from '../../../globals'
import { TableService } from '../../../services/integrated/table.service'
import {
    GET_CHILDREN_PAGES,
    GetChildrenPages,
    PAGES_GET_NEW_PRODUCTS,
    PagesGetNewProducts,
    PAGES_GET_HOT_NEWS,
    PagesGetHotNews,
    GET_PARTNERS_PIN,
    GetPartnersPin,
    PAGES_GET_BANNER,
    PagesGetBanner
} from '../../core/constants/api'
import { handleCodeRouter } from '../../core/constants/store'

@Component({
    selector: 'app-page',
    templateUrl: './page.component.html'
})
export class ListPageComponent implements OnInit, OnDestroy {
    private connect
    public data: any = {}
    public initState: number = -1
    private width: number = innerWidth
    public token: any = {
        getPage: 'api/getPageByLink',
        content: 'api/page/content'
    }
    language: string = ''
    childrenPages: any = {}

    public page = new TableService()

    constructor(public globals: Globals, public route: ActivatedRoute, public router: Router, public sanitizer: DomSanitizer) {
        this.router.events.subscribe(event => {
            if (event instanceof NavigationEnd) {
                window.scroll(0, 0)
            }
        })

        this.language = this.globals.language.getCode().toString() + '/'

        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case 'getPage':
                    if (res.status !== 1 || Object.values(res.data).length === 0) return

                    this.data = res.data

                    this.initState = 1

                    switch (+this.data.type) {
                        case 3:
                            this.products.send(res.data.id)
                            break
                        case 4:
                            this.contents.send(res.data.id)
                            break
                        default:
                            break
                    }

                    setTimeout(() => {
                        this.globals.htmlRender()
                    }, 500)
                    break
                case 'getDataProducts':
                    this.products.data = res.data
                    setTimeout(() => {
                        this.initState = res.data.length > 0 ? 1 : 0
                    }, 100)
                    break
                case 'getDataContents':
                    this.contents.data = res.data
                    setTimeout(() => {
                        this.initState = res.data.length > 0 ? 1 : 0
                    }, 100)
                    break

                case GetChildrenPages:
                    if (res.status !== 1 || Object.values(res.data).length === 0) return

                    this.childrenPages = res.data
                    break

                case PagesGetNewProducts:
                    if (res.status !== 1 || Object.values(res.data).length === 0) return

                    this.newProducts.data = res.data
                    break

                case PagesGetHotNews:
                    if (res.status !== 1 || Object.values(res.data).length === 0) return

                    this.hotNews.data = res.data
                    break

                case GetPartnersPin:
                    if (res.status !== 1 || res.data.length === 0) return

                    this.partners.data = res.data
                    break

                case PagesGetBanner:
                    if (res.status !== 1 || res.data.image.length === 0) return

                    this.banner.data = res.data
                    break

                default:
                    break
            }
        })
    }

    ngOnInit() {
        this.page._ini({
            data: [],
            keyword: 'getPage',
            count: this.Pagination.itemsPerPage,
            sorting: { field: 'maker_date', sort: 'DESC', type: 'date' }
        })
        this.route.params.subscribe(params => {
            let link = params.link
            this.globals.send({
                path: this.token.getPage,
                token: 'getPage',
                params: { link: link || '' }
            })

            this.globals.send({
                path: GET_CHILDREN_PAGES,
                token: GetChildrenPages,
                params: { link: link || '' }
            })
        })

        this.newProducts.send()

        this.hotNews.send()

        this.partners.send()

        this.banner.send()
    }

    ngOnDestroy() {
        this.connect.unsubscribe()
    }

    handleRouterPage = (code, parent_link, link) => {
        if (handleCodeRouter(code)) return

        this.router.navigate(['/' + this.language + (parent_link ? parent_link : link)])
    }

    newProducts = {
        data: <any>{},
        send: () =>
            this.globals.send({
                path: PAGES_GET_NEW_PRODUCTS,
                token: PagesGetNewProducts,
                params: { limit: 3 }
            })
    }

    hotNews = {
        data: <any>{},
        send: () =>
            this.globals.send({
                path: PAGES_GET_HOT_NEWS,
                token: PagesGetHotNews,
                params: { limit: 5 }
            })
    }

    partners = {
        data: [],
        send: () =>
            this.globals.send({
                path: GET_PARTNERS_PIN,
                token: GetPartnersPin,
                params: { limit: 5 }
            }),
        options: {
            items: this.width > 1024 ? 5 : this.width > 425 ? 4 : 3,
            autoplay: true,
            loop: true,
            dots: false
        }
    }

    public products = {
        token: 'api/page/products',
        data: [],
        send: id => {
            this.globals.send({ path: this.products.token, token: 'getDataProducts', params: { id: id || 0 } })
        }
    }

    public contents = {
        token: 'api/page/contents',
        data: [],
        send: id => {
            this.globals.send({ path: this.contents.token, token: 'getDataContents', params: { id: id || 0 } })
        }
    }

    banner = {
        data: <any>{},
        send: () => {
            this.globals.send({
                path: PAGES_GET_BANNER,
                token: PagesGetBanner,
                params: { type: 2 }
            })
        }
    }

    public Pagination = {
        maxSize: 5,

        itemsPerPage: 15,

        change: (event: PageChangedEvent) => {
            const startItem = (event.page - 1) * event.itemsPerPage

            const endItem = event.page * event.itemsPerPage

            this.page.data = this.page.cached.slice(startItem, endItem)

            var el = document.getElementById('ListData')

            window.scrollTo({
                top: 0,
                left: 0,
                behavior: 'smooth'
            })
        }
    }
}
