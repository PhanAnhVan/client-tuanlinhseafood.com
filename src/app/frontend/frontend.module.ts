import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { RouterModule, Routes } from '@angular/router'
import { TranslateModule } from '@ngx-translate/core'
import * as Services from '../services'
import * as Components from './components'
import { FrontendComponent } from './frontend.component'
import * as Layout from './layout'
import * as HomeLayout from './layout/home'
import * as Modules from './modules'
import * as Pages from './pages'
import { SanitizeHtmlPipe } from './sanitizeHtml.pipe'
import * as Shared from './shared'

const appRoutes: Routes = [
    {
        path: '',
        component: FrontendComponent,
        children: [
            { path: ':lang/trang-chu', redirectTo: '' },
            { path: ':lang/home', redirectTo: '' },
            { path: ':lang', component: Layout.HomeComponent },

            { path: ':lang/lien-he', component: Layout.ContactComponent },
            { path: ':lang/contact', component: Layout.ContactComponent },

            { path: ':lang/search', component: Layout.SearchComponent },
            { path: ':lang/search/:keywords', component: Layout.SearchComponent },

            { path: ':lang/:link', component: Pages.ListPageComponent },
            { path: ':lang/:parent_link/:link', component: Pages.ListPageComponent },
            { path: ':lang/:parent_links/:parent_link/:link', component: Pages.DetailPageComponent }
        ]
    }
]

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        TranslateModule,
        ReactiveFormsModule,
        RouterModule.forChild(appRoutes),
        Modules.TabsModule.forRoot(),
        Modules.TimepickerModule.forRoot(),
        Modules.BsDatepickerModule.forRoot(),
        Modules.ModalModule.forRoot(),
        Modules.CarouselModule,
        Modules.TypeaheadModule.forRoot(),
        Modules.PaginationModule.forRoot(),
        Modules.LazyLoadImageModule.forRoot({
            preset: Modules.intersectionObserverPreset
        }),
        Modules.Ng5SliderModule
    ],

    declarations: [
        FrontendComponent,
        Layout.HomeComponent,
        Layout.ContactComponent,
        Layout.SearchComponent,
        Layout.ProductListComponent,
        Layout.DetailProductComponent,
        Shared.HeaderComponent,
        Shared.FooterComponent,
        Shared.MenuComponent,
        Shared.MenuMobileComponent,
        HomeLayout.SlideComponent,
        HomeLayout.HomeProductsComponent,
        Pages.ListPageComponent,
        Pages.DetailPageComponent,
        Components.BoxContentComponent,
        Components.BoxContentGridComponent,
        Components.BoxProductComponent,
        Components.LoadingComponent,
        Components.BreadcrumbComponent,
        Components.BoxUpdatingComponent,
        Services.BindSrcDirective,
        SanitizeHtmlPipe
    ]
})
export class FrontendModule {}
