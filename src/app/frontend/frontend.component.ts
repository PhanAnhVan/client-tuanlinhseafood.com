import { AfterViewInit, Component } from '@angular/core'
import { NavigationEnd, Router } from '@angular/router'
import { TranslateService } from '@ngx-translate/core'
import { Globals } from '../globals'
import { ICON_ZALO } from './core/constants'

@Component({
    selector: 'app-frontend',
    templateUrl: './frontend.component.html',
    styleUrls: ['./frontend.component.scss']
})
export class FrontendComponent implements AfterViewInit {
    public connect
    public loading: boolean = false
    public lange = {}
    public token: any = {
        language: 'api/language'
    }
    iconZalo = ICON_ZALO

    constructor(public translate: TranslateService, public router: Router, public globals: Globals) {
        this.router.events.subscribe(event => {
            if (event instanceof NavigationEnd) {
                window.scroll(0, 0)
                this.globals.language.process()
                this.translate.use(this.globals.language.getCode().toString())
                this.globals.send({ path: this.token.language, token: 'language', params: { type: '0, 1' } })
            }
        })

        // gọi file lang lên vsf đưa vào vi.json
        let lang = {}
        this.connect = this.globals.result.subscribe((response: any) => {
            switch (response.token) {
                case 'language':
                    this.lange = response.data.reduce((n, o, i) => {
                        if (!n[o.text_key]) {
                            n[o.text_key] = o
                        }
                        return n
                    }, [])
                    setTimeout(() => {
                        for (let key in this.lange) {
                            lang[key] = this.lange[key]['value']
                            this.translate.set(key, this.lange[key]['value'])
                        }
                    }, 300)
                    break
                default:
                    break
            }
        })
    }

    ngAfterViewInit() {
        if (window['scrolltop']) {
            window['scrolltop']()
        }
    }

    eventLoading = e => {
        this.loading = true
        setTimeout(() => {
            this.loading = false
        }, 1000)
    }
}
