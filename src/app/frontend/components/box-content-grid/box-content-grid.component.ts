import { Component, Input } from '@angular/core'

@Component({
    selector: 'box-news-horizontal',
    templateUrl: './box-content-grid.component.html',
    styleUrls: ['./box-content-grid.component.scss']
})
export class BoxContentGridComponent {
    @Input('input') item
    @Input('language') language: string

    constructor() {}

    ngOnInit() {}
}
