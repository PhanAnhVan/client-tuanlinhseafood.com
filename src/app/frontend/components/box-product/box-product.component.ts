import { Component, Input, OnInit } from '@angular/core'
import { Globals } from '../../../globals'

@Component({
    selector: 'box-product-component',
    templateUrl: './box-product.component.html',
    styleUrls: ['./box-product.component.scss']
})
export class BoxProductComponent implements OnInit {
    @Input('item') item: any
    constructor(public globals: Globals) {}

    ngOnInit() {}
}
