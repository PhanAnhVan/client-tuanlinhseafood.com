// NGX-Bootstrap Modules
export { ModalModule } from 'ngx-bootstrap/modal'
export { BsDatepickerModule, PaginationModule, TabsModule, TimepickerModule } from 'ngx-bootstrap'
export { TypeaheadModule } from 'ngx-bootstrap/typeahead'

// ngx-owl-carousel-o
export { CarouselModule } from 'ngx-owl-carousel-o'

// lazyload
export { intersectionObserverPreset, LazyLoadImageModule } from 'ng-lazyload-image'

// Ng5Slider
export { Ng5SliderModule } from 'ng5-slider'
