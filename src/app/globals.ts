import { Injectable } from '@angular/core'
import { Http, Response } from '@angular/http'
import { Router } from '@angular/router'
import { TranslateService } from '@ngx-translate/core'
import { Subject } from 'rxjs'
import 'rxjs/add/operator/map'
import { GetStorage, Languages, SetStorage } from './frontend/core/constants/store'

export class configOptions {
    path: string
    data?: Object
    params?: Object
    token: string
}

@Injectable()
export class Globals {
    public BASE_API_URL = 'https://tuanlinhseafood.com/'

    public admin: string = 'admin'
    private response = new Subject<any>()
    public result = this.response.asObservable()
    public configCkeditor = {
        filebrowserBrowseUrl: this.BASE_API_URL + 'public/ckfinder/ckfinder.html',
        filebrowserImageBrowseUrl: this.BASE_API_URL + 'public/ckfinder/ckfinder.html?type=Images',
        filebrowserFlashBrowseUrl: this.BASE_API_URL + 'public/ckfinder/ckfinder.html?type=Flash',
        filebrowserUploadUrl: this.BASE_API_URL + 'public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
        filebrowserImageUploadUrl: this.BASE_API_URL + 'public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
        filebrowserFlashUploadUrl: this.BASE_API_URL + 'public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',
        littleFunction: {
            toolbar: [
                {
                    name: 'basicstyles',
                    items: ['Bold', 'Italic', 'Underline']
                },
                {
                    name: 'paragraph',
                    items: ['-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']
                },
                { name: 'links', items: ['Link'] },
                { name: 'styles', items: ['Format', 'Font', 'FontSize'] },
                { name: 'colors', items: ['TextColor', 'BGColor'] },
                { name: 'tools', items: ['Maximize'] },
                { name: 'document', items: ['Source'] }
            ],
            height: 67
        }
    }

    constructor(public translate: TranslateService, private http: Http, public router: Router) {}
    send = (option: configOptions) => {
        if (option.path && option.token) {
            // kiểm tra token
            let params = () => {
                let param = '?mask=' + option.token
                let link = window.location.pathname.split('/')[1] || ''
                let language = link === this.admin ? +this.language.get(true) : +this.language.get(false)
                param += '&language=' + language
                if (option.params) {
                    let keys = Object.keys(option.params)
                    for (let i = 0; i < keys.length; i++) {
                        if (i == 0) {
                            param += '&'
                        }
                        param += keys[i] + '=' + option.params[keys[i]]
                        param += i + 1 == keys.length ? '' : '&'
                    }
                }
                return param
            }
            this.http
                .post(this.BASE_API_URL + option.path + params(), option.data)
                .map((response: Response) => response.json())
                .subscribe((result: any) => {
                    this.response.next(result)
                })
        }
    }

    public time = {
        format: () => {},
        date: e => {
            e = typeof e === 'object' ? e : new Date()
            return e.getFullYear() + '-' + (e.getMonth() + 1).toString() + '-' + e.getDate()
        }
    }
    public USERS = {
        token: 'users',
        store: 'localStorage',
        get: skip => {
            if (skip == true) {
                return window.localStorage.getItem(this.USERS.store) ? JSON.parse(window.localStorage.getItem(this.USERS.store)) : {}
            } else {
                return window.localStorage.getItem(this.USERS.token) ? window.localStorage.getItem(this.USERS.token) : null
            }
        },
        check: skip => {
            return skip == true
                ? window.localStorage.getItem(this.USERS.store)
                    ? true
                    : false
                : window.localStorage.getItem(this.USERS.token)
                ? true
                : false
        },
        set: (data, skip) => {
            data = typeof data === 'object' ? JSON.stringify(data) : data
            if (skip == true) {
                window.localStorage.setItem(this.USERS.store, data)
            } else {
                window.localStorage.setItem(this.USERS.token, data)
            }
        },
        remove: (skip: any = '') => {
            if (!skip) {
                window.localStorage.clear()
            } else {
                if (skip == true) {
                    window.localStorage.removeItem(this.USERS.store)
                } else {
                    window.localStorage.removeItem(this.USERS.token)
                }
            }

            this.router.navigate(['/login/'])

            return this.http
                .post(this.BASE_API_URL + 'api/logoutadmin', {})
                .map((response: Response) => response.json())
                .subscribe((result: any) => {
                    this.response.next(result)
                })
        },

        _check: () => {
            return this.http.post(this.BASE_API_URL + 'api/checklogin', this.USERS.get(true)).map(response => response.json())
        }
    }

    public language = {
        skip: true,
        token: 'language', // number
        tokenCode: 'languageCode', // string
        tokenAdmin: 'languageAdmin', // number
        store: 'localStoragelanguage', // object
        get: (skip: boolean) => {
            if (skip) {
                return window.localStorage.getItem(this.language.tokenAdmin) ? window.localStorage.getItem(this.language.tokenAdmin) : 0
            } else {
                return window.localStorage.getItem(this.language.token) ? window.localStorage.getItem(this.language.token) : 0
            }
        },
        getCode: () => {
            return window.localStorage.getItem(this.language.tokenCode) ? window.localStorage.getItem(this.language.tokenCode) : 0
        },
        setCode: item => {
            item = typeof item === 'object' ? JSON.stringify(item) : item
            window.localStorage.setItem(this.language.tokenCode, item)
        },
        set: (item, skip) => {
            item = typeof item === 'object' ? JSON.stringify(item) : item
            if (skip == true) {
                window.localStorage.setItem(this.language.tokenAdmin, item)
            } else {
                window.localStorage.setItem(this.language.token, item)
            }
        },
        check: skip => {
            if (skip == true) {
                return window.localStorage.getItem(this.language.tokenAdmin) ? true : false
            } else {
                return window.localStorage.getItem(this.language.token) ? true : false
            }
        },
        getData: () => GetStorage(this.language.store),
        setData: (data: []) => SetStorage(this.language.store, data),
        process: () => {
            let data = GetStorage(this.language.store) || Languages.value

            let link = location.pathname.split('/')[1]

            let language = this.language.get(false)

            let router = ''

            let active: any = {}

            if (link) {
                data.filter(item => {
                    item.code == link ? (active = item) : {}
                    return item
                })
                router = window.location.pathname
            } else {
                data.filter(item => {
                    ;+item.id == +language ? (active = item) : {}
                    return item
                })
                active.code ? (router = '/' + active.code) : (router = '/' + Languages.default)
            }

            let format = /[%]/
            this.language.set(active.id, false)

            this.language.setCode(active.code)
            link == this.admin || link == 'login' || format.test(router) ? '' : this.router.navigate([router])
        }
    }

    public htmlRender = () => {
        const pageDetail = document.getElementById('page-detail')
        if (!pageDetail) return
        const elm = pageDetail.querySelectorAll('table')
        if (!elm.length) return

        const elmLength = elm.length
        for (let i = 0; i < elmLength; i++) {
            let container = document.createElement('div')
            container.className = 'table-responsive table-bordered m-0 border-0'
            elm[i].parentNode.insertBefore(container, elm[i])
            elm[i].className = 'table'
            elm[i].setAttribute('class', 'table')
            let rendered = "<table class='table'>" + elm[i].innerHTML + '</table>'
            elm[i].remove()
            container.innerHTML = rendered
        }
    }
}
