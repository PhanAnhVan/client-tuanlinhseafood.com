import { Component, OnInit, OnDestroy, ViewContainerRef } from '@angular/core'
import { TableService } from '../../../services/integrated/table.service'
import { BsModalService, BsModalRef } from 'ngx-bootstrap'
import { Globals } from '../../../globals'
import { AlertComponent } from '../../modules/alert/alert.component'
import { ToastrService } from 'ngx-toastr'
import { Router } from '@angular/router'

@Component({
    selector: 'app-getlist',
    templateUrl: './getlist.component.html'
})
export class GetlistComponent implements OnInit, OnDestroy {
    public connect: any

    public type: number = 0

    modalRef: BsModalRef

    public id: number

    public token: any = {
        getlist: 'get/pages/getlist', //router lấy dữ liệu

        remove: 'set/pages/remove',

        changestatus: 'set/pages/changestatus'
    }
    private cols = [
        { title: 'lblStt', field: 'index', show: true },
        { title: 'lblName', field: 'name', show: true, filter: true },
        { title: 'lblparent_name', field: 'parent_name', show: true, filter: true },
        { title: 'lblMaker_date', field: 'maker_date', show: true, filter: true },
        { title: '#', field: 'action', show: true }
    ]
    public cwstable = new TableService()

    constructor(
        private modalService: BsModalService,

        public router: Router,

        public toastr: ToastrService,

        public globals: Globals
    ) {
        this.cwstable._ini({ cols: this.cols, data: [], keyword: 'getListPages', count: 50 })

        this.connect = this.globals.result.subscribe((response: any) => {
            switch (response['token']) {
                case 'getListPages':
                    this.cwstable.data = []

                    this.cwstable.sorting = { field: 'maker_date', sort: 'DESC', type: '' }

                    this.cwstable._concat(response['data'], true)

                    break
                case 'removePages':
                    let type = response['status'] == 1 ? 'success' : response['status'] == 0 ? 'warning' : 'danger'

                    this.toastr[type](response['message'], type, { timeOut: 1000 })

                    if (response['status'] == 1) {
                        this.cwstable._delRowData(this.id)
                    }
                    break
                case 'changestatus':
                    let request = response.status == 1 ? 'success' : response.status == 0 ? 'warning' : 'danger'

                    this.toastr[request](response.message, request, { timeOut: 1000 })

                    if (response.status == 1) {
                        this.getList()
                    }
                    break
                default:
                    break
            }
        })
    }

    ngOnInit() {
        this.getList()
    }
    getList() {
        this.globals.send({ path: this.token.getlist, token: 'getListPages', params: { type: 2 } })
    }
    ngOnDestroy() {
        this.connect.unsubscribe()
    }
    onRemove(item: any): void {
        this.id = item.id

        this.modalRef = this.modalService.show(AlertComponent, { initialState: { messages: 'pages.removePages', name: item.name } })

        this.modalRef.content.onClose.subscribe(result => {
            if (result == true) {
                this.globals.send({ path: this.token.remove, token: 'removePages', params: { id: item.id } })
            }
        })
    }
    changeStatus = (id, status) => {
        this.globals.send({ path: this.token.changestatus, token: 'changestatus', params: { id: id, status: status == 1 ? 0 : 1 } })
    }
}
