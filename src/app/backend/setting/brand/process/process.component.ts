import { Component, OnInit, OnDestroy } from '@angular/core'
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { Router, ActivatedRoute } from '@angular/router'
import { ToastrService } from 'ngx-toastr'
import { Globals } from '../../../../globals'
import { uploadFileService } from '../../../../services/integrated/upload.service'
@Component({
    selector: 'app-process',
    templateUrl: './process.component.html'
})
export class BrandProcessComponent implements OnInit, OnDestroy {
    public id: number = 0

    public connect
    public token: any = {
        process: 'set/brand/process',
        getrow: 'get/brand/getrow'
    }
    fm: FormGroup

    public images = new uploadFileService()

    constructor(
        public fb: FormBuilder,

        private router: Router,

        private routerAct: ActivatedRoute,

        public toastr: ToastrService,

        private globals: Globals
    ) {
        this.connect = this.globals.result.subscribe((response: any) => {
            switch (response.token) {
                case 'processBrand':
                    let type = response.status == 1 ? 'success' : response.status == 0 ? 'warning' : 'danger'
                    this.toastr[type](response.message, type, { closeButton: true }, { timeOut: 1000 })
                    if (response.status == 1) {
                        setTimeout(() => {
                            this.router.navigate([this.globals.admin + '/settings/brand/get-list'])
                        }, 2000)
                    }
                    break
                case 'getRowBrand':
                    this.fmConfigs(response.data)
                    break
                default:
                    break
            }
        })
    }

    ngOnInit() {
        this.routerAct.params.subscribe(params => {
            this.id = +params['id']
            if (this.id && this.id > 0) {
                this.getRow()
            } else {
                this.fmConfigs()
            }
        })
    }
    ngOnDestroy() {
        this.connect.unsubscribe()
    }
    getRow() {
        this.globals.send({ path: this.token.getrow, token: 'getRowBrand', params: { id: this.id } })
    }
    fmConfigs(item: any = '') {
        item = typeof item === 'object' ? item : { status: 1, parent_id: 0, orders: 0 }
        this.fm = this.fb.group({
            name: [item.name ? item.name : '', [Validators.required]],
            orders: +item.orders ? +item.orders : 0,
            note: item.note ? item.note : '',
            status: item.status && +item.status == 1 ? true : false
        })

        const imagesConfig = { path: this.globals.BASE_API_URL + 'public/brand/', data: item.images ? item.images : '' }
        this.images._ini(imagesConfig)
    }
    onSubmit() {
        if (this.fm.valid) {
            const obj = this.fm.value
            obj.status === true ? (obj.status = 1) : (obj.status = 0)
            this.globals.send({ path: this.token.process, token: 'processBrand', data: obj, params: { id: this.id || 0 } })
        }
    }
}
