import { Component, OnInit, OnDestroy } from '@angular/core'
import { BsModalRef, BsModalService } from 'ngx-bootstrap'
import { TableService } from '../../../../services/integrated/table.service'
import { Globals } from '../../../../globals'
import { ToastrService } from 'ngx-toastr'
import { AlertComponent } from '../../../modules/alert/alert.component'

@Component({
    selector: 'app-brandlist',
    templateUrl: './get-list.component.html'
})
export class BrandListComponent implements OnInit, OnDestroy {
    modalRef: BsModalRef

    private connect

    public id

    public token: any = {
        getlist: 'get/brand/getlist',
        remove: 'set/brand/remove'
    }
    private cols = [
        { title: 'lblStt', field: 'index', show: true },
        { title: 'lblName', field: 'name', show: true, filter: true },
        { title: 'lblMaker_date', field: 'maker_date', show: true, filter: true },
        { title: 'lblAction', field: 'action', show: true },
        { title: '#', field: 'status', show: true, filter: true }
    ]
    public cwstable = new TableService()

    constructor(private modalService: BsModalService, public globals: Globals, public toastr: ToastrService) {
        this.connect = this.globals.result.subscribe((response: any) => {
            switch (response.token) {
                case 'getlistbrand':
                    this.cwstable.sorting = { field: 'maker_date', sort: 'DESC', type: '' }

                    this.cwstable._concat(response.data, true)

                    break
                case 'removebrand':
                    let type = response.status == 1 ? 'success' : response.status == 0 ? 'warning' : 'danger'

                    this.toastr[type](response.message, type, { closeButton: true }, { timeOut: 1000 })

                    if (response.status == 1) {
                        setTimeout(() => {
                            this.cwstable._delRowData(this.id)
                        }, 1000)
                    }
                default:
                    break
            }
        })
    }

    ngOnInit() {
        this.getlist()
    }
    ngOnDestroy() {
        this.connect.unsubscribe()
    }
    getlist = () => {
        this.globals.send({ path: this.token.getlist, token: 'getlistbrand' })
        this.cwstable._ini({ cols: this.cols, data: [], count: 10 })
    }

    onRemove(item) {
        this.id = item.id

        this.modalRef = this.modalService.show(AlertComponent, { initialState: { messages: 'brand.remove', name: item.name } })

        this.modalRef.content.onClose.subscribe(result => {
            if (result == true) {
                this.globals.send({ path: this.token.remove, token: 'removebrand', params: { id: item.id } })
            }
        })
    }
}
