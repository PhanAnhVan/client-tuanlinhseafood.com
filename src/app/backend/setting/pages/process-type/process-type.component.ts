import { Component, OnInit } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { ActivatedRoute, Router } from '@angular/router'
import { TranslateService } from '@ngx-translate/core'
import { ToastrService } from 'ngx-toastr'
import { Globals } from '../../../../globals'
import { LinkService } from '../../../../services/integrated/link.service'
import { TagsService } from '../../../../services/integrated/tags.service'
import { uploadFileService } from '../../../../services/integrated/upload.service'

@Component({
    selector: 'app-process-type',
    templateUrl: './process-type.component.html'
})
export class ProcessTypeComponent implements OnInit {
    fm: FormGroup

    public connect: any

    public token: any = {
        process: 'set/pages/process',

        pathGetRowPages: 'get/pages/getrow',

        pathGetPages: 'get/pages/getlist'
    }
    public id: number

    public required: boolean = false

    public listPages: any = []

    public images = new uploadFileService()

    public icons = new uploadFileService()

    public type: number = 0

    constructor(
        public fb: FormBuilder,

        public router: Router,

        public toastr: ToastrService,

        public globals: Globals,

        private routerAct: ActivatedRoute,

        public translate: TranslateService,

        public tags: TagsService,

        public link: LinkService
    ) {
        switch (this.router.url.split('/')[3]) {
            case 'link':
                this.type = 1
                break
            case 'product':
                this.type = 3
                break
            case 'content':
                this.type = 4
                break
            case 'library':
                this.type = 5
                break
            case 'customer':
                this.type = 6
                break
            case 'service':
                this.type = 7
                break
            default:
                break
        }
        this.getListPages()
        this.routerAct.params.subscribe(params => {
            this.id = +params['id']
            if (this.id && this.id != 0) {
                this.getRow()
            } else {
                this.fmConfigs()
            }
        })

        this.connect = this.globals.result.subscribe((response: any) => {
            switch (response['token']) {
                case 'GetRowPages':
                    let data = response['data']
                    this.fmConfigs(data)
                    break

                case 'PagesProcess':
                    let type = response['status'] == 1 ? 'success' : response['status'] == 0 ? 'warning' : 'danger'
                    this.toastr[type](response['message'], type, { timeOut: 1000 })
                    if (response['status'] == 1) {
                        setTimeout(() => {
                            this.router.navigate([this.globals.admin + '/settings/' + this.router.url.split('/')[3] + '/get-list'])
                        }, 2000)
                    }
                    break

                case 'getpagesPages':
                    this.listPages = response.data
                    break
                default:
                    break
            }
        })
    }
    ngOnInit() {
        this.getListPages()
    }
    ngOnDestroy() {
        this.connect.unsubscribe()
    }
    getListPages() {
        this.globals.send({ path: this.token.pathGetPages, token: 'getpagesPages', params: { type: this.type } })
    }
    fmConfigs(item: any = '') {
        item = typeof item === 'object' ? item : { status: 1, parent_id: 0, orders: 0 }

        this.fm = this.fb.group({
            name: [item.name ? item.name : '', [Validators.required]],

            orders: +item.orders ? +item.orders : 0,

            link: item.link ? item.link : '',

            title: item.title ? item.title : '',

            parent_id: +item.parent_id ? +item.parent_id : 0,

            detail: item.detail ? item.detail : '',

            description: item.description ? item.description : '',

            keywords: item.keywords ? item.keywords : '',

            status: item.status && item.status == 1 ? true : false
        })

        const imagesConfig = { path: this.globals.BASE_API_URL + 'public/pages/', data: item.images ? item.images : '' }
        const iconsConfig = { path: this.globals.BASE_API_URL + 'public/pages/', data: item.icon ? item.icon : '' }

        this.images._ini(imagesConfig)
        this.icons._ini(iconsConfig)

        this.tags._set(item.keywords ? JSON.parse(item['keywords']) : '')
    }
    onChangeLink(e) {
        if (+this.type != 1) {
            const url = this.link._convent(e.target.value)
            this.fm.value.link = url
        }
    }
    getRow() {
        this.globals.send({ path: this.token.pathGetRowPages, token: 'GetRowPages', params: { id: this.id } })
    }
    onSubmit() {
        if (this.fm.valid) {
            let data: any = this.fm.value

            data.status = data.status == true ? 1 : 0

            data.keywords = this.tags._get()

            data.type = this.type

            data.link = data.type == 1 ? data.link : this.link._convent(data.link)

            data.images = this.images._get(true)

            data.icon = this.icons._get(true)

            this.globals.send({ path: this.token.process, token: 'PagesProcess', data: data, params: { id: this.id || 0 } })
        }
    }
}
