import { Component, OnInit, OnDestroy } from '@angular/core'
import { BsModalRef, BsModalService } from 'ngx-bootstrap'
import { ActivatedRoute, Router } from '@angular/router'

import { TableService } from '../../../../services/integrated/table.service'
import { ToastrService } from 'ngx-toastr'
import { Globals } from '../../../../globals'
import { AlertComponent } from '../../../modules/alert/alert.component'

@Component({
    selector: 'app-get-list-type',
    templateUrl: './get-list-type.component.html'
})
export class GetListTypeComponent implements OnInit, OnDestroy {
    public connect: any

    public type: number = 0

    public translateTitle: string = ''

    modalRef: BsModalRef

    public id: number

    public token: any = {
        getlist: 'get/pages/getlist', //router lấy dữ liệu

        remove: 'set/pages/remove',

        changestatus: 'set/pages/changestatus',

        changePin: 'set/pages/changePin'
    }
    private cols = [
        { title: 'lblStt', field: 'index', show: true },
        { title: 'lblName', field: 'name', show: true, filter: true },
        { title: 'lblparent_name', field: 'parent_name', show: true, filter: true },
        { title: 'lblMaker_date', field: 'maker_date', show: true, filter: true },
        { title: 'lblAction', field: 'action', show: true }
    ]
    public cwstable = new TableService()

    constructor(
        private modalService: BsModalService,

        public routerAtc: ActivatedRoute,

        public router: Router,

        public toastr: ToastrService,

        public globals: Globals
    ) {
        this.cwstable._ini({ cols: this.cols, data: [], keyword: 'getListPages', count: 50 })

        this.translateTitle = this.router.url.split('/')[3]

        switch (this.router.url.split('/')[3]) {
            case 'link':
                this.type = 1
                break
            case 'product':
                this.type = 3
                break
            case 'content':
                this.type = 4
                break
            case 'library':
                this.type = 5
                break
            case 'customer':
                this.type = 6
                break
            case 'service':
                this.type = 7
                break
            default:
                break
        }
        this.getList(this.type)

        this.connect = this.globals.result.subscribe((response: any) => {
            switch (response['token']) {
                case 'getListPages':
                    this.cwstable.data = []
                    this.cwstable.sorting = { field: 'maker_date', sort: 'DESC', type: '' }
                    this.cwstable._concat(response['data'], true)
                    break
                case 'removePages':
                    let type = response['status'] == 1 ? 'success' : response['status'] == 0 ? 'warning' : 'danger'
                    this.toastr[type](response['message'], type, { timeOut: 1000 })
                    if (response['status'] == 1) {
                        this.cwstable._delRowData(this.id)
                    }
                    break
                case 'changestatus':
                case 'changePin':
                    let request = response.status == 1 ? 'success' : response.status == 0 ? 'warning' : 'danger'
                    this.toastr[request](response.message, request, { timeOut: 1000 })
                    if (response.status == 1) {
                        this.getList(this.type)
                    }
                    break

                default:
                    break
            }
        })
    }

    ngOnInit() {}
    getList(type) {
        this.globals.send({ path: this.token.getlist, token: 'getListPages', params: { type: type } })
    }
    ngOnDestroy() {
        this.connect.unsubscribe()
    }
    onRemove(item: any): void {
        this.id = item.id

        this.modalRef = this.modalService.show(AlertComponent, { initialState: { messages: 'pages.removePages', name: item.name } })

        this.modalRef.content.onClose.subscribe(result => {
            if (result == true) {
                this.globals.send({ path: this.token.remove, token: 'removePages', params: { id: item.id } })
            }
        })
    }

    changeStatus = (id, status) => {
        this.globals.send({ path: this.token.changestatus, token: 'changestatus', params: { id: id, status: status == 1 ? 0 : 1 } })
    }
}
