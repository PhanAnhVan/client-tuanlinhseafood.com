import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { SettingComponent } from './setting.component'
import { Routes, RouterModule } from '@angular/router'
import { TranslateModule } from '@ngx-translate/core'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { AlertModule } from 'ngx-bootstrap/alert'
import { EmailComponent } from './email/email.component'
import { WebsiteComponent } from './website/website.component'
import { TagsService } from '../../services/integrated/tags.service'
import { OrtherComponent } from './orther/orther.component'
import { TimepickerModule } from 'ngx-bootstrap/timepicker'
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker'
import { CKEditorModule } from 'ckeditor4-angular'

export const routes: Routes = [
    {
        path: '',
        component: SettingComponent,
        children: [
            { path: '', redirectTo: 'website' },
            { path: 'slide', loadChildren: () => import('./slide/slide.module').then(m => m.SlideModule) },
            { path: 'email', component: EmailComponent },
            { path: 'website', component: WebsiteComponent },
            { path: 'orther', component: OrtherComponent },
            { path: 'menu', loadChildren: () => import('./menu/menu.module').then(m => m.SettingsMenuModule) },
            { path: 'content', loadChildren: () => import('./pages/pages.module').then(m => m.SetiingsPagesModule) },
            // { path: 'library', loadChildren: () => import('./pages/pages.module').then(m => m.SetiingsPagesModule) },
            // { path: 'link', loadChildren: () => import('./pages/pages.module').then(m => m.SetiingsPagesModule) },
            { path: 'customer', loadChildren: () => import('./pages/pages.module').then(m => m.SetiingsPagesModule) },
            { path: 'origin', loadChildren: () => import('./origin/origin.module').then(m => m.OriginModule) },
            { path: 'brand', loadChildren: () => import('./brand/brand.module').then(m => m.BrandModule) },
            { path: 'product', loadChildren: () => import('./pages/pages.module').then(m => m.SetiingsPagesModule) },
            { path: 'company', component: OrtherComponent },
            { path: 'social-network', component: OrtherComponent }
        ]
    }
]
@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        FormsModule,
        TranslateModule,
        ReactiveFormsModule,
        AlertModule.forRoot(),
        TimepickerModule.forRoot(),
        BsDatepickerModule.forRoot(),
        CKEditorModule
    ],
    providers: [EmailComponent, WebsiteComponent, OrtherComponent, TagsService],
    declarations: [SettingComponent, EmailComponent, WebsiteComponent, OrtherComponent]
})
export class SettingModule {}
