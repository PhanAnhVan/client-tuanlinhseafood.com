import { Component, OnInit, OnDestroy, ViewContainerRef } from '@angular/core'
import { TableService } from '../../../../services/integrated/table.service'
import { Globals } from '../../../../globals'
import { BsModalService, BsModalRef } from 'ngx-bootstrap'
import { AlertComponent } from '../../../modules/alert/alert.component'
import { ToastrService } from 'ngx-toastr'

@Component({
    selector: 'app-get-list',
    templateUrl: './get-list.component.html'
})
export class MenuGetListComponent implements OnInit, OnDestroy {
    public connect: any

    modalRef: BsModalRef

    public id: number = 0

    public token: any = {
        getlist: 'get/pagesgroup/getlist', //router lấy dữ liệu

        remove: 'set/pagesgroup/remove'
    }
    private cols = [
        { title: 'lblStt', field: 'index', show: true },
        { title: 'lblName', field: 'name', show: true, filter: true },
        { title: 'lblOrders', field: 'orders', show: false, filter: true },
        { title: 'lblMaker_date', field: 'maker_date', show: true, filter: true },
        { title: 'lblAction', field: 'action', show: true },
        { title: '#', field: 'status', show: true, filter: true }
    ]
    public table = new TableService()
    constructor(
        public globals: Globals,

        private modalService: BsModalService,

        public toastr: ToastrService
    ) {
        this.connect = this.globals.result.subscribe((response: any) => {
            switch (response['token']) {
                case 'getListPagesGroup':
                    this.table._concat(response['data'], true)
                    break
                case 'removePagesGroup':
                    let type = response['status'] == 1 ? 'success' : response['status'] == 0 ? 'warning' : 'danger'
                    this.toastr[type](response['message'], type, { timeOut: 1000 })
                    if (response['status'] == 1) {
                        this.table._delRowData(this.id)
                    }
                    break
                default:
                    break
            }
        })
    }
    ngOnInit() {
        this.getList()
        this.table._ini({ cols: this.cols, data: [], keyword: 'getListPagesGroup' })
    }
    getList() {
        this.globals.send({ path: this.token.getlist, token: 'getListPagesGroup' })
    }
    ngOnDestroy() {
        this.connect.unsubscribe()
    }
    onRemove(item: any): void {
        this.id = item.id
        this.modalRef = this.modalService.show(AlertComponent, {
            initialState: { messages: 'pagesGroup.removePagesGroup', name: item.name }
        })
        this.modalRef.content.onClose.subscribe(result => {
            if (result == true) {
                this.globals.send({ path: this.token.remove, token: 'removePagesGroup', params: { id: item.id } })
            }
        })
    }
}
