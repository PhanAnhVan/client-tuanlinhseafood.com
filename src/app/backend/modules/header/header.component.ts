import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Globals } from '../../../globals';
import { TranslateService } from '@ngx-translate/core';
@Component({
    selector: 'admin-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

    @Output('eventOpened') eventOpened = new EventEmitter<number>();
    @Output('eventLang') eventLang = new EventEmitter<boolean>();

    public company: any; connect;
    public step = 0;
    public expanded: boolean = true;
    public mutiLang: any = { active: {}, data: [] };
    public user: any = {}
    public token: any = {
        company: "api/company",
    }
    constructor(
        public globals: Globals,
        public translate: TranslateService,
    ) {
        //language- ngôn ngữ
        let language = this.globals.language.get(true);
        this.mutiLang.data = this.globals.language.getData();

        if (this.mutiLang.data.length > 0) {
            this.mutiLang.data.filter(item => {
                if (+item.id == +language) {
                    this.mutiLang.active = item
                }
                return item
            })
        };
        //user - tài khoản
        this.user = this.globals.USERS.get(true);
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case 'company':
                    this.company = res.data;
                    break;

                default:
                    break;

            }
        });
    }
    ngOnInit() {
        this.globals.send({ path: this.token.company, token: 'company' });
    }
    onClickMenu = () => {
        this.eventOpened.emit()
    }
    onLanguage = (language_id) => {
        this.mutiLang.data.filter(item => {
            if (+item.id == +language_id) {
                this.mutiLang.active = item
            }
            return item
        })
        this.globals.language.set(language_id, true);
        this.eventLang.emit()
    }
}

