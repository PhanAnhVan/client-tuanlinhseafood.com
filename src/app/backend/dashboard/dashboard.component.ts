import { Component, OnDestroy, OnInit } from '@angular/core'
import { TranslateService } from '@ngx-translate/core'
import { Globals } from '../../globals'

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit, OnDestroy {
    public data = []
    public connect: any

    public token: any = {
        getDashboard: 'get/dashboard/getlist'
    }

    constructor(public globals: Globals, public translate: TranslateService) {
        this.connect = this.globals.result.subscribe((response: any) => {
            switch (response['token']) {
                case 'getDashboard':
                    this.data = response.data
                    break

                default:
                    break
            }
        })
    }
    ngOnInit() {
        this.globals.send({
            path: this.token.getDashboard,
            token: 'getDashboard'
        })
    }

    ngOnDestroy() {
        this.connect.unsubscribe()
    }
}
