import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { TooltipModule } from 'ngx-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ModalModule, AlertModule } from 'ngx-bootstrap';
import { CKEditorModule } from 'ckeditor4-angular';
import { GetlistComponent } from './getlist/getlist.component';
import { GroupgetlistComponent } from './groupgetlist/groupgetlist.component';
import { ProcessContentComponent } from './process/process.component';
import { MainComponent } from './main/main.component';

const appRoutes: Routes = [

	{ path: '', redirectTo: 'get-list' },
	{ path: 'get-list', component: MainComponent },
	{ path: 'insert', component: ProcessContentComponent },
	{ path: 'update/:id', component: ProcessContentComponent },

]

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		RouterModule.forChild(appRoutes),
		ModalModule.forRoot(),
		AlertModule.forRoot(),
		TranslateModule,
		CKEditorModule,
		TooltipModule.forRoot()
	],
	declarations: [
		GetlistComponent,

		GroupgetlistComponent,
		ProcessContentComponent,
		MainComponent,
	],

})
export class ContentsModule { }
