import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { ToastrService } from 'ngx-toastr'
import { Globals } from '../../../globals'
import { LinkService } from '../../../services/integrated/link.service'
import { TagsService } from '../../../services/integrated/tags.service'
import { uploadFileService } from '../../../services/integrated/upload.service'
import { isNgTemplate } from '@angular/compiler'

@Component({
    selector: 'app-process',
    templateUrl: './process.component.html'
})
export class ProcessProductComponent implements OnInit {
    public id: number = 0
    private connect
    public fm: FormGroup
    // public price: FormGroup
    public seo: FormGroup
    // public validPrice: boolean = false
    private token: any = {
        process: 'set/products/process',
        update: 'set/products/update',
        getrow: 'get/products/getrow',
        getlistproductgroup: 'get/pages/grouptype',
        getlistorigin: 'get/origin/getlist',
        getlistbrand: 'get/brand/getlist'
    }

    public data = { origin: [], brand: [], group: [] }

    // public listtype = [

    //     { title: "products.productsHilight", check: false, value: 1 },

    // ];

    public images = new uploadFileService()

    public listimages = new uploadFileService()

    constructor(
        private routerAct: ActivatedRoute,
        public router: Router,
        public globals: Globals,
        private link: LinkService,
        private fb: FormBuilder,
        public tags: TagsService,
        private toastr: ToastrService
    ) {
        this.routerAct.params.subscribe(params => {
            this.id = +params['id']
            if (this.id && this.id > 0) {
                this.globals.send({ path: this.token.getrow, token: 'getrow', params: { id: this.id } })
            } else {
                this.fmConfigs()
                // this.priceConfigs()
                this.seoConfigs()
                this.productImages.set()
            }
        })

        this.connect = this.globals.result.subscribe((response: any) => {
            switch (response.token) {
                case 'getrow':
                    let data = response.data
                    // this.listtype = this.listtype.filter(item => {
                    //     item.check = (data.type.search(item.value.toString()) !== -1) ? true : false;
                    //     return true;
                    // })
                    this.fmConfigs(data)
                    // this.priceConfigs(data)
                    this.seoConfigs(data)
                    this.productImages.set(data)
                    break

                case 'getlistorigin':
                    this.data.origin = response.data
                    break

                case 'getlistbrand':
                    this.data.brand = response.data
                    break

                case 'getlistproductgroup':
                    this.data.group = response.data
                    break

                case 'processProduct':
                    this.id = response.data
                case 'updateImages':
                case 'updateProduct':
                    let type = response.status == 1 ? 'success' : response.status == 0 ? 'warning' : 'danger'
                    this.toastr[type](response.message, type, { closeButton: true }, { timeOut: 1000 })
                    if (response.status == 1) {
                        setTimeout(() => {
                            this.router.navigate([this.globals.admin + '/products/update/' + this.id])
                        }, 1000)
                    }
                    break
                default:
                    break
            }
        })
    }
    ngOnInit() {
        this.globals.send({ path: this.token.getlistproductgroup, token: 'getlistproductgroup', params: { type: 3 } })
        this.globals.send({ path: this.token.getlistorigin, token: 'getlistorigin' })
        this.globals.send({ path: this.token.getlistbrand, token: 'getlistbrand' })
    }

    onChangeLink(e) {
        const url = this.link._convent(e.target.value)
        this.fm.value.link = url
    }
    ngOnDestroy() {
        this.connect.unsubscribe()
    }

    fmConfigs(item: any = '') {
        item = typeof item === 'object' ? item : { status: 1, page_id: 0 }

        this.fm = this.fb.group({
            name: [item.name ? item.name : '', [Validators.required]],
            link: [item.link ? item.link : '', [Validators.required]],
            page_id: [item.page_id ? item.page_id : '', [Validators.required]],
            origin_id: item.origin_id ? +item.origin_id : 0,
            brand_id: item.brand_id ? +item.brand_id : 0,
            info: item.info ? item.info : '',
            detail: item.detail ? item.detail : '',
            status: item.status && +item.status == 1 ? true : false
        })
    }

    onSubmit() {
        const obj = this.fm.value
        // obj.type = JSON.stringify(Object.keys(this.listtype.reduce((n, o, i) => {
        //     if (o.check == true) {
        //         n[o.value] = o.value;
        //     }
        //     return n;
        // }, {})));
        obj.status === true ? (obj.status = 1) : (obj.status = 0)
        this.globals.send({ path: this.token.process, token: 'processProduct', data: obj, params: { id: this.id || 0 } })
    }

    // priceConfigs(item: any = '') {
    //     item = typeof item === 'object' ? item : {}
    //     this.price = this.fb.group({
    //         bound: [item.bound ? item.bound : 0],
    //         number: [item.number ? item.number : 0],
    //         price_sale: [item.price_sale ? item.price_sale : 0],
    //         price_buy: [item.price_buy ? item.price_buy : 0],
    //         // percent: [item.percent ? +item.percent : 0, [Validators.max(100), Validators.min(0),]],
    //         vat: item.vat ? +item.vat : 0
    //     })
    // }
    // onConfigPrice = skip => {
    //     let price_sale = +this.price.value.price_sale.toString().split('.').join('')
    //     let price_buy = +this.price.value.price_buy.toString().split('.').join('')

    //     if (skip) {
    //         if (price_sale > 0 && price_sale < price_buy) {
    //             this.price.value.percent = (100 - (price_sale * 100) / price_buy).toFixed()
    //         }
    //     } else {
    //         let value = new Number((price_buy * (100 - this.price.value.percent)) / 100).toLocaleString('vn').split(',').join('.')
    //         this.price.value.price_sale = value
    //     }
    // }
    // updatePrice = () => {
    //     const obj = {
    //         bound: +this.price.value.bound,
    //         number: +this.price.value.number,
    //         percent: +this.price.value.percent,
    //         price_sale: +this.price.value.price_sale.toString().split('.').join(''),
    //         price_buy: +this.price.value.price_buy.toString().split('.').join(''),
    //         vat: 0
    //     }
    //     if (+obj.price_sale <= +obj.price_buy) {
    //         this.globals.send({ path: this.token.update, token: 'updateProduct', data: obj, params: { id: this.id || 0 } })
    //     } else {
    //         this.validPrice = true
    //     }
    // }
    seoConfigs(item: any = '') {
        item = typeof item === 'object' ? item : { status: 1, page_id: 0 }
        this.seo = this.fb.group({
            description: item.description ? item.description : ''
        })

        this.tags._set(item.keywords ? JSON.parse(item['keywords']) : '')
    }
    updateSeo = () => {
        const obj = this.seo.value
        obj.keywords = this.tags._get()
        this.globals.send({ path: this.token.update, token: 'updateProduct', data: obj, params: { id: this.id || 0 } })
    }

    public productImages = {
        tokenProcess: 'set/products/updateImages',
        set: (item: any = '') => {
            item = typeof item === 'object' ? item : { images: '', listimages: '' }
            const imagesConfig = { path: this.globals.BASE_API_URL + 'public/products/', data: item.images ? item.images : '' }
            this.images._ini(imagesConfig)
            const listimagesConfig = {
                path: this.globals.BASE_API_URL + 'public/products/',
                data: item.listimages ? item.listimages : '',
                multiple: true
            }
            this.listimages._ini(listimagesConfig)
        },

        process: () => {
            if (this.id > 0) {
                let data: any = {}

                data.images = this.images._get(true)

                data.listimages = this.listimages._get(true)

                this.globals.send({
                    path: this.productImages.tokenProcess,
                    token: 'updateImages',
                    data: data,
                    params: { id: this.id > 0 ? this.id : 0 }
                })
            }
        }
    }
}
