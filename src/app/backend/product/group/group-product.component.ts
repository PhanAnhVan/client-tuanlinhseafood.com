import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core'
import { BsModalRef } from 'ngx-bootstrap'
import { Globals } from '../../../globals'
import { TableService } from '../../../services/integrated/table.service'

@Component({
    selector: 'app-group-product',
    templateUrl: './group-product.component.html',
    styleUrls: ['./group-product.component.css']
})
export class GroupProductGetlistComponent implements OnInit, OnDestroy {
    @Output('filter') filter = new EventEmitter()

    public connect
    modalRef: BsModalRef
    public tablegroupproduct = new TableService()
    public show
    public id

    private cols = [
        { title: 'lblGroup', field: 'name', show: true, filter: true },
        { title: 'lblCount', field: 'count_product', show: true, filter: true }
    ]

    constructor(public globals: Globals) {
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case 'getlistproductgroup':
                    this.tablegroupproduct._concat(res.data, true)
                    break

                default:
                    break
            }
        })
    }

    ngOnInit() {
        this.getlist()
    }

    ngOnDestroy() {
        this.connect.unsubscribe()
    }

    getlist = () => {
        this.globals.send({ path: 'get/pages/grouptype', token: 'getlistproductgroup', params: { type: 3 } })
        this.tablegroupproduct._ini({ cols: this.cols, data: [] })
    }

    onCheckItem(item) {
        item.check = item.check == true ? false : true
        this.filter.emit(item.id)
    }
}
