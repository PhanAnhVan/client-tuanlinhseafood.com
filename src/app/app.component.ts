import { Component, OnDestroy, OnInit } from '@angular/core'
import { Meta, Title } from '@angular/platform-browser'
import { Router } from '@angular/router'
import { TranslateService } from '@ngx-translate/core'
import { GetCompanyInfo, GET_COMPANY_INFO } from './frontend/core/constants/api'
import { CompanyInfo, Languages, SetStorage } from './frontend/core/constants/store'
import { Globals } from './globals'

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html'
})
export class AppComponent implements OnInit, OnDestroy {
    public connect
    public loading: boolean = false
    public token = {
        mutiLanguage: 'api/mutiLanguage'
    }

    constructor(
        public translate: TranslateService,
        private globals: Globals,
        private title: Title,
        private meta: Meta,
        public router: Router
    ) {
        this.translate.setDefaultLang('vi')
        this.translate.use('vn')

        this.connect = this.globals.result.subscribe((response: any) => {
            switch (response.token) {
                case GetCompanyInfo:
                    if (Object.values(response.data).length) {
                        let data = response.data

                        SetStorage(CompanyInfo, data)

                        this.title.setTitle(data.name)
                        this.meta.addTag({ name: 'description', content: data.description })
                        this.meta.addTag({ name: 'keywords', content: data.keywords })
                        let shortcut = document.querySelector("[type='image/x-icon']")
                        if (shortcut && data.shortcut) {
                            shortcut.setAttribute('href', data.shortcut)
                        }
                    }
                    break

                case 'mutiLang':
                    let data = response.data
                    if (data && data.length > 0) {
                        let languageCode = []

                        response.data.reduce((n, o, i) => {
                            languageCode.push(o.code)
                            return n
                        }, [])
                        this.translate.addLangs(languageCode)

                        Languages.value = data

                        this.globals.language.setData(data)
                    } else {
                        this.globals.send({
                            path: this.token.mutiLanguage,
                            token: 'mutiLang'
                        })
                    }
                    break
                default:
                    break
            }
        })
    }

    ngOnInit() {
        this.globals.send({
            path: this.token.mutiLanguage,
            token: 'mutiLang'
        })

        this.globals.send({
            path: GET_COMPANY_INFO,
            token: GetCompanyInfo
        })
    }

    ngOnDestroy() {
        this.connect.unsubscribe()
    }
}
