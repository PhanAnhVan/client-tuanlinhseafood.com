Fancybox.bind('[data-fancybox]', {
    Thumbs: false,
    Toolbar: false,

    Image: {
        zoom: false,
        click: false,
        wheel: 'slide'
    }
})

function libraryGallary() {
    var $container = $('.animate-grid .gallary-thumbs')
    $container.isotope({
        filter: '*',
        animationOptions: {
            duration: 750,
            easing: 'linear',
            queue: false
        }
    })
    $('.animate-grid .categories a').click(function () {
        $('.animate-grid .categories .active').removeClass('active')
        $(this).addClass('active')
        var selector = $(this).attr('data-filter')
        $container.isotope({
            filter: selector,
            animationOptions: {
                duration: 750,
                easing: 'linear',
                queue: false
            }
        })
        return false
    })
}

function scrolltop() {
    $(window).on('scroll', function () {
        if ($(this).scrollTop() > 200) {
            $('.scrollTop').fadeIn(200)
        } else {
            $('.scrollTop').fadeOut(200)
        }
    })
    $('.scrollTop').on('click', function () {
        $('html, body').animate({ scrollTop: 0 }, 1500)
        return false
    })
}

$(document).ready(function () {
    $('body').tooltip({ selector: '[data-toggle=tooltip]' })
})
